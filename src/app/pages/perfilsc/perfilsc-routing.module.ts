import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PerfilscPage } from './perfilsc.page';

const routes: Routes = [
  {
    path: '',
    component: PerfilscPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PerfilscPageRoutingModule {}
