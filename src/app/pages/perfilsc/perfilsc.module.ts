import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PerfilscPageRoutingModule } from './perfilsc-routing.module';

import { PerfilscPage } from './perfilsc.page';
import { ComponentsModule } from '../../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PerfilscPageRoutingModule,
    ComponentsModule
  ],
  declarations: [PerfilscPage]
})
export class PerfilscPageModule {}
